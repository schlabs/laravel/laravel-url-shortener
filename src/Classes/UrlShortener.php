<?php
namespace SchLabs\LaravelUrlShortener\Classes;

use Illuminate\Support\Facades\Http;

class UrlShortener
{

    private string $authToken;
    private string $domain;

    public function __construct(string $authToken, string $domain)
    {
        $this->authToken = $authToken;
        $this->domain = $domain;

        return $this;
    }

    /**
     * ShortIo URL shortener provider
     * 
     * @var string $url URL to short
     * @return string Short URL
     */
    public function shortIo(string $url)
    {
        $response = Http::withHeaders([
            "authorization" => $this->authToken,
            "content-type" => "application/json"
        ])
        ->post('https://api.short.io/links', [
            'originalURL' => $url,
            'domain' => $this->domain
        ]);

        return data_get($response->json(), 'shortURL', '');
    }

}