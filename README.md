
# laravel-url-shortener
  

## Description

Url shortener providers for Schlabs projects
  

## Depencencies

* 
  

## Installation
  

**In your composer.json**

Add the following code to the "repositories" key:

```json
"repositories": [
	{
		"type": "vcs",
		"url": "https://gitlab.com/schlabs/laravel/laravel-url-shortener"
	}
]
```

Add this line to your require dependecies:

```json
"schlabs/laravel-url-shortener": "1.0.*"
```
 
**In your project root run:**
```sh
composer update
```

## Usage
